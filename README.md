# Exercise6
Коллекции и дженерики 

#Содержание файла "Track.java"

public class Track {

	public Track() {
		
	}

}

#Содержание файла "CompactDisc.java"

public class CompactDisc extends Media {
	
	public CompactDisc() {
	
	}
	
}

#Содержание файла "Library.java"

public class Library<T> {

	public Library() {
		
	}

}



#Инструкция по запуску проекта

#Скопируйте указанный ниже код
#Вставьте с ПОЛНОЙ заменой в файл tasks.json
#Расположение файла tasks.json: Explorer => первая папка ".theia" => tasks.json

{
    "tasks": [
        {
            "type": "che",
            "label": "OnlineMedia build",
            "command": "mvn clean install",
            "target": {
                "workingDir": "${CHE_PROJECTS_ROOT}/Exercise6",
                "component": "maven"
            },
            "problemMatcher": []
        },
        {
            "type": "che",
            "label": "OnlineMedia build and run",
            "command": "mvn clean install && java -jar ./target/*.jar",
            "target": {
                "workingDir": "${CHE_PROJECTS_ROOT}/Exercise6",
                "component": "maven"
             },
            "problemMatcher": []
        },
        {
            "type": "che",
            "label": "OnlineMedia test",
            "command": "mvn verify",
            "target": {
                "workingDir": "${CHE_PROJECTS_ROOT}/Exercise6",
                "component": "maven"
             },
            "problemMatcher": []
        }
    ]
}
